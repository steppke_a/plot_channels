# plot_channels

Basic plotter for EPICS or BS-based channels with output to either the terminal console or a web interface based on NiceGUI and Plotly.

````  
Usage: plot_channels [OPTIONS] CHANNEL_NAMES...

  Plot channel data from BS (beam synchronous) or PV (EPICS) sources. Use
  TEST_CHANNEL for test data.

Options:
  --acquire-interval FLOAT  Interval in seconds for data collection.
  --plot-interval INTEGER   Interval in seconds for plot update.
  --save-prefix TEXT        Prefix for the save filenames.
  --in-subplots             Enable plotting each channel in a subplot.
  --relative-time           Use relative time since start of acquisition.
  --web                     Whether to use the local web interface as a GUI.
  --help                    Show this message and exit.
````

# Installation

At the moment the easiest is in a conda environment with the necessary requirements (numpy, plotly, nicegui, plotext). In the future we will pack this additionally into a binary using pyinstaller.

# Screenshots
Everyone loves pictures, so here is the terminal interface:

![terminal](terminal_interface.png)

and from the web interface:

![web](web_interface.png)