from setuptools import setup, find_packages

setup(
    name='plot_channels',
    version='0.1',
    packages=['plot_channels'],
    package_dir={'': 'src'},
    install_requires=['bsread', 'epics', 'nicegui', 'numpy', 'plotly',],
    author='Alexander Steppke',
    author_email='alexander.steppke@psi.ch',
    description='Parallel processing of SwissFEL data using SFDataFiles',
    url='https://gitlab.psi.ch/steppke_a/plot_channels',
)
